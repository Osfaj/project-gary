const { write } = require("fs");
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

world = () => {


    let chooseName = "";
    let chooseUmur = "";
    let pattUmur =/[0-9]/g;
    let chooseDaftarKerja = "";
    let loading = '------------------------\n\"Harap Mengisi Form!!!\"\n';
    let loading2 = '------------------------\n\"Harap Mengisi Form Dengan Benar!!!\"\n';
    let kerjaHabis = ''
    // Tempat masukin nama.
    // let arrCV = [ //Opsi CV
        // {
            // CV: "Input CV",
        // }
    // ];

    // let arrKerja = [ //Search Pekerjaan
        // {
            // Kerja: "Mulai",
        // }
    // ];

    giveName = () => { 
        console.clear()
        rl.question('\n\n\"SELAMAT DATANG DI PT MENCARI KERJA\"\n\n\nTolong masukan namamu : ', function (name) { 
        if (name.length <= 2){
            console.log(loading);
            giveName()
        }
        else{
            chooseName = name;
            giveUmur();
        }
        });
    }

    giveUmur = (name) =>{
        rl.question("\n\nMasukan umur kamu : ", function (umur) {
            if (umur.match(pattUmur)){
                chooseUmur = umur;
                giveCv ();
            }   
            else{
                console.log(' __________________________\n\n|Masukan umur dengan benar!|\n|__________________________|');
                giveUmur();
            }
        });
    }

    giveCv = (umur) => {
        console.clear();
        rl.question("\n\nApakah kamu memiliki CV? (Ketik \"Input CV\" jika punya | \Ketik \"N\" jika tidak) : ", function (cv) {
            switch (cv) {
                case "Input CV":
                    // result();
                    setTimeout(function(){result(); }, 1000);
                    break;
                case "input cv":
                    // result();
                    setTimeout(function(){result(); }, 1000);
                    break;
                case "N" :
                    giveTanyaCv();
                    break;
                default :
                    // 
                    console.log('\n\n\"Mohon pilih dengan benar\!!!"');
                    giveCv();
                    break;
            }
        })
    }

    giveTanyaCv = (Cv) => {
        rl.question("\n\n\nIngin membuat CV? <Y/N> : ", function (TanyaCv) {
            switch (TanyaCv) {
                case "Y":
                    giveMasukanCv();
                    break;
                case "N":
                    giveName();
                    break;
                default :
                    console.log('\n\n\"Mohon pilih dengan benar!!!\n"\n\n\n');
                    // giveTanyaCv();
                    setTimeout(function(){giveTanyaCv(); }, 1000);
                    break;
            }
        })
    }

    giveMasukanCv = (TanyaCv) => {
        rl.question("\n\nKetik \"input cv\" untuk membuat CV : ", function (masukanCv) {
            switch (masukanCv) {
                case "input cv":
                    // giveDaftarKerja();
                    setTimeout(function(){result(); }, 1000);
                    break;
                default :
                    console.log('\"Mohon berikan input yang benar!!!\"');
                    givemasukanCv();
                    break;
            }
        })
    }

    result = (name, masukanCv) => {
        // console.log(varibel());
        console.log("\n\n\n\"Halo " + chooseName + " " + chooseUmur + " Tahun," + " terimakasih sudah bergabung.\"\n\n\n");
        // giveKerja();
        setTimeout(function(){giveKerja(); }, 1000);
    }

    giveKerja = () => {
        console.clear();
        console.log('\nHALAMAN UTAMA PT. MENCARI KERJA\n\nMENU:\n1. Profile \n2. Log Out')
        rl.question("\nKetik \"Mulai\" untuk mencari pekerjaan : ", function (kerja) {
            switch (kerja) {
                case "Mulai":
                    // giveDaftarKerja();
                    setTimeout(function(){giveDaftarKerja(); }, 1000);
                    break;
                case "1":
                    // giveDaftarKerja();
                    setTimeout(function(){giveProfilKerja(); }, 1000);
                    break;
                case "2":
                    // giveDaftarKerja();
                    setTimeout(function(){giveName(); }, 1000);
                    break;
                default :
                    console.log('\"Kami tidak memahami maksud pencarian anda!!!\"');
                    giveKerja();
                    break;
            }
        })
    }
    
    giveProfilKerja = (kerja) => {
            console.clear();
            console.log('\nPROFILE USER PT. MENCARI KERJA\n\nMENU:\n'+ chooseName + ', ' + chooseUmur + ' Tahun'+ '\n\n1. Back')
            rl.question("Masukan Input : ", function(ProfilKerja) {
            switch(ProfilKerja){
                case '1':
                    giveKerja();
                    // setTimeout(function(){; }, 1000);
                    // giveDaftarKerja();
                    break;
                default :
                    console.log('\"Mohon pilih dengan benar\!!!"\n\n\n');
                    setTimeout(function(){giveDaftarKerja(); }, 1000);
                    break;
                }
                
            });         
        }
        


    giveDaftarKerja = (kerja) => {
    console.clear();
    rl.question("\"PEKERJAAN DITEMUKAN!\"\n\nDAFTAR PEKERJAAN :\n1. Front End Developer\n2. Back End Developer\n3. React Native\n\n\n\"Pilih pekerjaan\" : ", function(daftarkerja) {
        switch(daftarkerja){
            case '1':
                givePekerja();
                // setTimeout(function(){; }, 1000);
                // giveDaftarKerja();
                break;
            case '2':
                console.clear();
                console.log('\n\n\n"Mohon maaf, pekerjaan yang anda cari sudah diambil..."\n\n\n');
                setTimeout(function(){giveDaftarKerja(); }, 2500);
                // setTimeout(function(){; }, 1000);
                // giveDaftarKerja();
                break;
            case '3':
                console.clear();
                console.log('\n\n\n\"Mohon maaf, pekerjaan yang anda cari sudah diambil..."\n\n\n');
                setTimeout(function(){giveDaftarKerja(); }, 2500);
                break;
            default :
                console.log('\n\n\n"Mohon pilih dengan benar\!!!"\n\n\n')
                setTimeout(function(){giveDaftarKerja(); }, 1000);
                break;
            }         
        });         
    }

    givePekerja = (daftarkerja) => {
        console.clear();
        console.log('\n\n\n\n"LAMARAN UNTUK PEKERJAAN FRONT END DEVELOPER\"')
        rl.question("\n\"Ingin memasukan lamaran?\n\"<Y/N>\" : ", function(pekerja) {
            switch(pekerja){
                case 'Y':
                    console.clear();
                    console.log('\n\n\n"Lamaran ditambahkan, kami akan memberi notifikasi kepada kamu jika diterima"\n\n\n');
                    setTimeout(function(){giveKerja(); }, 2500);
                    // setTimeout(function(){; }, 1000);
                    // giveDaftarKerja();
                    break;
                case 'N':
                    console.clear();
                    console.log('\n\n\n\"Kamu membatalkan lamaran"\n\n\n');
                    setTimeout(function(){giveKerja(); }, 2500);
                    // setTimeout(function(){; }, 1000);
                    // giveDaftarKerja();
                    break;
                default :
                    console.log('\n\n\n\n\n\"Mohon pilih dengan benar\!!!"\n\n\n')
                    setTimeout(function(){giveDaftarKerja(); }, 1000);
                    break;
                }         
            });         
        }

    giveName();
}

world();
