var animasi = bodymovin.loadAnimation({
    container: document.getElementById('bawah'),
    renderer: 'svg',
    loop : false,
    autoplay : true,
    path: '/footer/animasi.json'
})

// var pengunjung = localStorage.getItem('nama');
// document.getElementById('visitor').value = pengunjung;
// console.log(document.getElementById('visitor').value);
// document.getElementsByClassName('visitor').innerHTML = localStorage.getItem('nama');
// console.log(document.getElementsByClassName('visitor').innerHTML);
if (localStorage.getItem('nama')) {
    setTimeout(function(){document.getElementsByClassName('visitor')[0].innerHTML = "Halloo " + localStorage.getItem("nama") + ", selamat datang!";}, 2400);
}

var tombol = document.getElementById("tombolnya");
var hex = ['#F8F6E9', '#F2E0C9', '#FFE0CF'];

const warna = () =>{
    var randomColor = hex[Math.floor(Math.random() * hex.length)];
    document.body.style.backgroundColor = randomColor;
}
tombol.addEventListener('click', warna);