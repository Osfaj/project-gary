class Student {
    constructor(name, age, date, gender, id, hobbies) {
        this.name = name;
        this.age = age;
        this.date = date;
        this.gender = gender;
        this.id = id;
        this.hobbies = hobbies;
    }

    setName(name) {
        this.name = name;
    }
    setAge(age) {
        this.age = age;
    }
    setDate(date) {
        this.date = date;
    }
    setGender(gender) {
        if (gender === 'Male' || gender === 'Female') {
            this.gender = gender;
        }
    }
    setHobbies(hobbies) {
        this.hobbies.push(hobbies)
    }

    setRemove = (hobbies) => {
        let i = this.hobbies.findIndex((x => x) == "");
        if (i < hobbies.length) {
            this.hobbies.splice(hobbies);
        }
    }

    getData = () => {
        return `
        Name : ${this.name} 
        Age : ${this.age} 
        Date : ${this.date} 
        Gender : ${this.gender}
        Id Student : ${this.id} 
        Hobbies : ${this.hobbies}
        `;
    }
}

const user = new Student(
    'Gary',
     24,
    '04/04/1996',
    'Male',
    '0092020048',
    ['Nonton', 'Rollerblading', 'Gaming']
);



console.log(user.getData());





