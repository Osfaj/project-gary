import React from 'react';
import {Button, Form, FormGroup, Label, Input, FormText} from 'reactstrap';
import style from './Assets/todo-list.module.css';

const Todolist = () => {
    return (
        <div class={style.body}>
            <div className={style.container}>
                <h1>To do List.</h1>
                    <Form>
                        <div className={style.inputtext}>
                            <Label className={style.labeltext} for="exampleText">Tambah agenda</Label><br/>
                            <Input className={style.textbox} type="textarea" name="text" id="exampleText" />
                            <div className={style.tempattombol}>
                                <Button className={style.buttontambah} color="primary" size="lg">+</Button>{' '}
                                <Button className={style.buttonkurang} color="primary" size="lg">-</Button>{' '}
                            </div>
                        </div>
                        
                        <ul className={style.listlabelnya}>
                            <li/><Label check className={style.labelnya}>
                                <Input className={style.checkbox1} type="checkbox" />{' '}
                                Beli pakan bebek
                            </Label>
                            <li/><Label check className={style.labelnya}>
                                <Input className={style.checkbox2} type="checkbox" />{' '}
                                Beli pakan lele
                            </Label>
                            <li/><Label check className={style.labelnya}>
                                    <Input className={style.checkbox3} type="checkbox" />{' '}
                                    Beli makan siang
                            </Label>
                        </ul>
                    </Form>
            </div>
        </div>
    )
}

export default Todolist;