import React, {Component} from 'react';
//import logo from './logo.svg';
import './App.css';
// import Coba from './components/Hello'
import Satu from './components/satu';
import Dua from './components/dua';
import Tiga from './components/tiga';
import Empat from './components/empat';
import Lima from './components/lima';
import Enam from './components/enam';
import Tujuh from './components/tujuh';
import Delapan from './components/delapan';
import Sembilan from './components/sembilan';
import Sepuluh from './components/sepuluh';

class App extends Component{
    render(){
        return(
            <div className = "App">
                <Satu/><br/>
                <Dua/><br/>
                <Tiga/><br/>
                <Empat/><br/>
                <Lima/><br/>
                <Enam/><br/>
                <Tujuh/><br/>
                <Delapan/>
                <Sembilan/>
                <Sepuluh/>
            </div>    
        );
    }
}

export default App;