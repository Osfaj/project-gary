import React from 'react'
import style from "./asset/lima.module.css"

const Lima = () => {
    return (
        <div class={style.body}>
            <header class={style.header}><h1 contenteditable>Header.com</h1></header>
            <div class={style.leftsidebar} contenteditable>Left Sidebar</div>
            <main class={style.main} contenteditable></main>
            <div class={style.rightsidebar} contenteditable>Right Sidebar</div>
            <footer class={style.footer}contenteditable>Footer Content — Header.com 2020</footer>
        </div>
    )
}

export default Lima;