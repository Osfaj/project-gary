import React from 'react'
import style from "./asset/dua.module.css"

const Dua = () =>{
    return (
        <div className={style.parent}>
            <div className={style.child}>1</div>
            <div className={style.child}>2</div>
            <div className={style.child}>3</div>
        </div>
    )
}

export default Dua;