import React from 'react'
import style from "./asset/sepuluh.module.css"

const Sepuluh = () => {
    return (
        <div className={style.body}>
            <div className={style.card}>
                <h1 className={style.h1}>Title Here</h1>
                <div className={style.visual}></div>
                <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
            </div>
        </div>
    )
}

export default Sepuluh;