import React from 'react'
import style from "./asset/tujuh.module.css"

const Tujuh = () => {
    return (
        <div className={style.body}>
            <div className={style.div}>1</div>
            <div className={style.div}>2</div>
            <div className={style.div}>3</div>
            <div className={style.div}>4</div>
        </div>
    )
}

export default Tujuh;