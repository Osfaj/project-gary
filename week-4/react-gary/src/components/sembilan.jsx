import React from 'react'
import style from "./asset/sembilan.module.css"

const Sembilan = () => {
    return (
        <div className={style.body}>
            <div className={style.card}>
                <h1 className={style.h1}>Title Here</h1>
                <div className={style.visual}></div>
                <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
                <br/>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum eveniet beatae veritatis saepe corporis voluptates illo placeat maxime sapiente. Sit facere cumque quidem ad quo, dolores pariatur repudiandae ullam animi?</p>
            </div>
        </div>
    )
}

export default Sembilan;