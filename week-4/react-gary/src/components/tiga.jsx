import React from 'react'
import style from "./asset/tiga.module.css"

const Tiga = () =>{
    return (
      <div className={style.body}>  
        <div className={style.sidebar} contenteditable>
        Min: 150px
        <br/>
        Max: 25%
        </div>
            <p className={style.content} contenteditable>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis nulla architecto maxime modi nisi. Quas saepe dolorum, architecto quia fugit nulla! Natus, iure eveniet ex iusto tempora animi quibusdam porro?</p>
        </div>
    )
}

export default Tiga;