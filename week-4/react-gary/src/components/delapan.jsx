import React from 'react'
import style from "./asset/delapan.module.css"

const Delapan = () => {
    return (
        <div className={style.body}>
            <div className={style.card}>
                <h1 class={style.h1}>Title - Card 1</h1>
                <p>Medium length description. Let's add a few more words here.</p>
                <div className={style.visual}></div>
            </div>
            <div className={style.card}>
                <h1 class={style.h1}>Title - Card 2</h1>
                <p>Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
                <div className={style.visual}></div>
            </div>
            <div className={style.card}>
                <h1 class={style.h1}>Title - Card 3</h1>
                <p>Short Description.</p>
                <div className={style.visual}></div>
            </div>
        </div>
    )
}

export default Delapan;