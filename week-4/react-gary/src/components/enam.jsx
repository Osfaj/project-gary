import React from 'react'
import style from "./asset/enam.module.css"

const Enam = () => {
    return (
        <div className={style.body}>
            <div className={style.span12}>Span 12</div>
            <div className={style.span6}>Span 6</div>
            <div className={style.span4}>Span 4</div>
            <div className={style.span2}>Span 2</div>
        </div>
    )
}

export default Enam;